from manim import *

class CircleAnim(Scene):
    def construct(self):
        circle = Circle()
        circle.set_fill(PINK, opacity=0.5)
        square = Square()
        square.rotate(PI / 4)
        square.set_fill(PINK, opacity=0.5)
        annot = MathTex("\mathbb{S}^1")
        annot.next_to(circle, RIGHT, buff = 0.5)

        self.play(Create(annot))
        self.play(Create(circle))
        self.play(Transform(circle, square))
        self.play(circle.animate.set_fill(BLUE, opacity=0.9))
        self.play(FadeOut(circle))